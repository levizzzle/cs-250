// Lab - Standard Template Library - Part 1 - Vectors
// FIRSTNAME, LASTNAME

#include <iostream>
#include <vector>
#include <string>
using namespace std;

int main()
{
	vector<string> courses;

	bool done = false;
	while (!done)
	{
		cout << endl << "MAIN MENU" << endl;
		cout << "Vector size: " << courses.size() << endl;
		cout << "1. Add a new course "
			<< "2. Remove last course "
			<< "3. Display course list "
			<< "4. Quit " << endl;

		int choice;
		cin >> choice;

		if (choice == 1) {
			string newCourse;
			cout << "What is the course name? ";
			cin >> newCourse;
			courses.push_back(newCourse);
		}
		else if (choice == 2) {
			courses.pop_back();
		}
		else if (choice == 3) {
			for (int i = 0; i < courses.size(); i++)
			{
				cout << i << ". " << courses[i] << endl;
			}
		}
		else if (choice == 4) {
			done = true;
		}
	}
    cin.ignore();
    cin.get();
    return 0;
}
