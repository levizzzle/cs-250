// Lab - Standard Template Library - Part 2 - Lists
// FIRSTNAME, LASTNAME

#include <iostream>
#include <list>
#include <string>
using namespace std;

void displayList(list<string>& states);

int main()
{
	list<string> states;
	bool done = false;

	while (!done) {
		cout << endl << "MAIN MENU " << endl
			<< "1. Add new state to the front" << endl
			<< "2. Add new state to the back " << endl
			<< "3. Pop state from the front " << endl
			<< "4. Pop state from the end " << endl
			<< "5. Continue " << endl;

		int choice;
		cin >> choice;
		string newState;

		if (choice == 1) {
			cout << "New State: ";
			cin >> newState;
			states.push_front(newState);
		}
		else if (choice == 2) {
			cout << "New State: ";
			cin >> newState;
			states.push_back(newState);
		}
		else if (choice == 3) {
			states.pop_front();
		}
		else if (choice == 4) {
			states.pop_back();
		}
		else if (choice == 5) {
			cout << "List of States: " << endl;
			displayList(states);
			cout << "List of States in reverse: " << endl;
			states.reverse(); 
			displayList(states);
			cout << "List of States sorted: " << endl;
			states.sort(); 
			displayList(states);
			cout << "List of States sorted and in reverse: " << endl;
			states.sort();
			displayList(states);
			states.reverse();
			displayList(states);
		}

	}

    cin.ignore();
    cin.get();
    return 0;
}

void displayList(list<string>& states) 
{
	for (list<string>::iterator it = states.begin();
		it != states.end();
		it++)
	{
		cout << *it << "\t";
	}
	cout << endl;
}