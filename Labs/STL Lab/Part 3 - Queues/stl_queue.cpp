// Lab - Standard Template Library - Part 3 - Queues
// FIRSTNAME, LASTNAME

#include <iostream>
#include <string>
#include <queue>
using namespace std;

int main()
{
	queue<float> transactions;
	bool done = false;
	
	while (!done) {
		cout << "1. Deposit or Withdraw" << endl
			<< "2. Continue ";
		int choice;
		cin >> choice;

		if (choice == 1) {
			float amount;
			cout << "Enter an amount: ";
			cin >> amount;
			transactions.push(amount);
		}
		else if (choice == 2) {
			float balance = 0;
			while (!transactions.empty()) {
				for (int i = 0; i < transactions.size(); i++) {
					balance += transactions.front();
					cout << "Transaction: " << balance << endl;
					transactions.pop();
				}
			}
			cout << "Balance: " << balance << endl;
		}
	}

    cin.ignore();
    cin.get();
    return 0;
}
