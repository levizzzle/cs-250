// Lab - Standard Template Library - Part 4 - Stacks
// FIRSTNAME, LASTNAME

#include <iostream>
#include <string>
#include <stack>
using namespace std;

int main()
{
	stack<string> word;
	bool done = false;
	cout << "Enter the next letter of a word" << endl
		<< "1. UNDO " << endl << "2. DONE " << endl;

	while (!done) {
		string letter;
		cout << ">> ";
		cin >> letter;
		//if (letter != "1" || letter != "2")
			//word.push(letter);
		if (letter == "1") {
			word.pop();
		}
		else if (letter == "2") {
			//word.pop();
			while (!word.empty()) {
				cout << word.top();
				word.pop();
				done = true;
			}
		}
		else
			word.push(letter);
	}


    cin.ignore();
    cin.get();
    return 0;
}
