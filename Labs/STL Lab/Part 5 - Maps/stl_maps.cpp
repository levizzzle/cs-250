// Lab - Standard Template Library - Part 5 - Maps
// FIRSTNAME, LASTNAME

#include <iostream>
#include <string>
#include <map>
using namespace std;



int main()
{
	map<char, string> colors;
	/*colors['r', 'g', 'b', 'c', 'm', 'y'] 
		= "FF0000", "00FF00", "0000FF", "00FFFF", "FF00FF", "FFFF00";*/
	colors['r'] = "FF0000";
	colors['g'] = "00FF00";
	colors['b'] = "0000FF";
	colors['c'] = "00FFFF";
	colors['m'] = "FF00FF";
	colors['y'] = "FFFF00";

	char input = ' ';
	cout << "Enter a character for a color (r, g, b, c, m, y, or \"q\" to quit)" << endl;
	while (input != 'q') {
		cin >> input;
		if (input == 'r' || input == 'g' || input == 'b' || 
			input == 'c' || input == 'm' || input == 'y') {
			cout << "Hex: " << colors[input] << endl;
		}
		else if (input == 'q') {
			cout << "Quitting... ";
		}
		else
			cout << "Invalid character selected, try again: " << colors[input];
	}

    cin.ignore();
    cin.get();
    return 0;
}
