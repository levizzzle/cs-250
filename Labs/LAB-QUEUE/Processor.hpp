#ifndef _PROCESSOR
#define _PROCESSOR

#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
using namespace std;

#include "Job.hpp"
#include "Queue.hpp"

class Processor
{
    public:
    void FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile );
    void RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile );
};

void Processor::FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile )
{
	ofstream output(logFile);
	output << "First come; First served" << endl;

	int cycles = 0;

	while (jobQueue.Size() > 0)
	{
		for (int i = 0; i < 34; i++) output << "-";
		output << endl << "Job ID:  " << jobQueue.Front()->id + 1 << setw(18) << "Processing..." << endl;
		for (int i = 0; i < 34; i++) output << "-";
		output << endl;

		while (!jobQueue.Front()->fcfs_done)
		{
			cycles++;
			output << "Cycle: " << setw(4) << cycles
				<< "    Remaining Time: " << jobQueue.Front()->fcfs_timeRemaining << endl;
			jobQueue.Front()->Work(FCFS);
		}
		jobQueue.Front()->SetFinishTime(cycles, FCFS);
		jobQueue.Pop();
	}
	
	int total = 0;
	for (int i = 0; i < allJobs.size(); i++)
	{
		for (int i = 0; i < 34; i++) output << "-";

		output << endl << "Job ID:" << setw(4) << allJobs[i].id + 1 << endl
			<< "Time to complete: " << setw(3) << allJobs[i].fcfs_finishTime << " cycles" << endl;
		total += allJobs[i].fcfs_finishTime;
	}
	output << endl << "Average time per job: " << total / allJobs.size() << " cycles" << endl
		<< "Total time: " << cycles << " cycles";
	output.close();
}

void Processor::RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile )
{
	ofstream output(logFile);
	output << "Round Robin" << endl;

	int cycles = 0;
	int timer = 0;

	for (int i = 0; i < 34; i++) output << "-";
	output << endl << "Job ID:  " << jobQueue.Front()->id + 1 << setw(18) << "Processing..." << endl;
	for (int i = 0; i < 34; i++) output << "-";
	output << endl;

	while (jobQueue.Size() > 0)
	{
		output << "Cycle: " << setw(4) << cycles
			<< "    Remaining Time: " << jobQueue.Front()->rr_timeRemaining << endl;
		jobQueue.Front()->Work(RR);
		if (timer == timePerProcess || jobQueue.Front()->rr_done)
		{
			if (timer == timePerProcess)
			{
				jobQueue.Front()->rr_timesInterrupted++;
				jobQueue.Push(jobQueue.Front());
				jobQueue.Pop();
				timer = 0;
			}
			if (jobQueue.Front()->rr_done)
			{
				jobQueue.Front()->SetFinishTime(cycles, RR);
				jobQueue.Pop();
			}
			if(jobQueue.Size() > 0)
			{
				for (int i = 0; i < 34; i++) output << "-";
				output << endl << "Job ID:  " << jobQueue.Front()->id + 1 << setw(18) << "Processing..." << endl;
				for (int i = 0; i < 34; i++) output << "-";
				output << endl;
			}
		}
		cycles++;
		timer++;
	}
			
	int total = 0;
	for (int i = 0; i < allJobs.size(); i++)
	{
		for (int i = 0; i < 34; i++) output << "-";

		output << endl << "Job ID:" << setw(4) << allJobs[i].id +1 << endl
			<< "Time to complete: " << setw(3) << allJobs[i].rr_finishTime << endl
			<< "Times interrupted: " << allJobs[i].rr_timesInterrupted << endl;

		total += allJobs[i].rr_finishTime;
	}
	output << endl << "Average time per job: " << total / allJobs.size() << " cycles" << endl
		<< "Total time: " << cycles << " cycles" << endl
		<< "Round Robin interval: " << timePerProcess;
	output.close();
}
#endif
