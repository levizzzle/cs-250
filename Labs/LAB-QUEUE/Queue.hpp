#ifndef QUEUE_HPP
#define QUEUE_HPP

#include "LinkedList.hpp"



template <typename T>
class Queue
{
    public:
    void Push( T data )
    {
		m_list.PushBack(data);
    }

    void Pop()
    {
		if (m_list.IsEmpty())
			return;
		else
			m_list.PopFront();
    }

    T& Front()
    {
			return m_list.GetFirst();
    }

    int Size()
    {
		return m_list.Size();
    }

    private:
	LinkedList<T> m_list;
};
#endif
