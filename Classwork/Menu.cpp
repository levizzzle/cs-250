#include <iostream>
#include <string>
using namespace std;


class Menu
{
	public:

		static void Header(const string& header)
		{
			DrawHorizontalBar(80);
			string head = "|| " + header + " ||";
			cout << " " << head << endl << " ";
			DrawHorizontalBar(head.size());
			cout << endl;
		}

		static void DrawHorizontalBar(int width, char symbol = '-')
		{
			for (int i = 0; i < width; i++)
			{
				cout << symbol;
			}
			cout << endl;
		}
};