#include<iostream>
using namespace std;

template<typename T>
class Node
{
public:
	Node()
	{
		ptrNext = NULL;
		ptrPrev = NULL;
	}
	T data;
	Node* ptrNext;
	Node* ptrPrev;
};

template <typename T>
class LinkedList
{
public:
	LinkedList()
	{
		ptrFront = NULL;
		ptrEnd = NULL;
		m_size = 0;
	}
	
	~LinkedList()
	{
		Clear();
	}

	void Clear()
	{
		while (!ptrFront = NULL)
		{
			PopBack();
		}
	}
	void PushBack(T data)
	{
		Node<T>* newNode = new Node<T>;
		newNode->data = data;

		if (m_size == 0)
		{
			ptrFirst = newNode;
			ptrLast = newNode;
		}
		else
		{
			newNode->ptrNext = ptrLast;
			ptrLast->ptrNext = newNode;
			ptrLast = newNode;
		}
		m_size++;
	}
	void PushFront(T data)
	{
		Node<T>* newNode = new Node<T>;
		newNode->data = data;

		if (m_size == 0)
		{
			ptrFirst = newNode;
			ptrLast = newNode;
		}
		else
		{
			newNode->ptrNext = ptrFirst;
			newNode->ptrPrev = NULL;
			ptrFirst->ptrPrev = newNode;
			ptrFirst = newNode;
		}
		m_size++;
	}
	void PopBack()
	{
		if (m_size == 0)
		{
			return;
		}
		else if (ptrFirst == ptrLast)
		{
			delete ptrFirst;
			ptrFirst = NULL;
			ptrLast = NULL;
			m_size = 0;
		}
		else
		{
			Node<T>* secondLast = ptrLast->ptrPrev;
			delete ptrLast;
			ptrLast = secondLast;
			ptrLast->ptrNext = NULL;
			m_size--;
		}
	}
	void PopFront()
	{
		if (m_size == 0)
		{
			return;
		}
		else if (ptrFirst == ptrLast)
		{
			ptrFirst = NULL;
			ptrLast = NULL;
		}
		else
		{
			Node<T>* secondFirst = ptrFirst->ptrNext;
			delete ptrFirst;
			ptrFirst = secondFirst;
			ptrFirst->ptrPrev = NULL;
		}
		m_size--;
	}

	T& GetFirst()
	{
		return ptrFirst->data;
	}
	T& GetLast()
	{
		return ptrLast->data;
	}
	T& At(int index)
	{
		int counter = 0;
		Node<T>* ptrCurrent = ptrFirst;
		while (counter < index)
		{
			ptrCurrent = ptrCurrent->ptrNext;
			counter++;
		}
		return ptrCurrent->data;
	}
	 

private:
	Node<T> ptrFirst;
	Node<T> ptrLast;
	int m_size;
};

