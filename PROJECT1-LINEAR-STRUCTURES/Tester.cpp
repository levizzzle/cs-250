#include "Tester.hpp"

void checkPass(char, char);
void checkPass(char*, char*);
void checkResult(bool, bool);

void Tester::RunTests()
{
    Test_IsEmpty();
    Test_IsFull();
    Test_Size();
    Test_GetCountOf();
    Test_Contains();

    Test_PushFront();
    Test_PushBack();

    Test_Get();
    Test_GetFront();
    Test_GetBack();

    Test_PopFront();
    Test_PopBack();
    Test_Clear();

    Test_ShiftRight();
    Test_ShiftLeft();

    Test_Remove();
    Test_Insert();
}

void Tester::DrawLine()
{
    cout << endl;
    for ( int i = 0; i < 80; i++ )
    {
        cout << "-";
    }
    cout << endl;
}

void Tester::Test_Init()
{
    DrawLine();
    cout << "TEST: Test_Init" << endl;

}

void Tester::Test_ShiftRight()
{
    DrawLine();
    cout << "TEST: Test_ShiftRight" << endl;

	{   // Test begin
		cout << endl << "Test 1 - Create List, ShiftRight(1), Get(1)" << endl;
		List<char> testList;
		bool expectedResult = false;
		bool actualResult = testList.ShiftRight(1);
		char *expectedReturn = nullptr;
		char *actualReturn = testList.Get(1);

		cout << "Expected return: nullptr" << endl;
		cout << "Actual return:   ";
		if (actualReturn == nullptr)
			cout << "nullptr" << endl;
		else
			cout << actualReturn << endl;
		
		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end

	{   // Test begin
		cout << endl << "Test 2 - Create List, PushBack(A, B, C), ShiftRight(2), Get(2)" << endl;
		List<char> testList;
		testList.PushBack('A');
		testList.PushBack('B');
		testList.PushBack('C');
		bool expectedResult = true;
		bool actualResult = testList.ShiftRight(2);
		char expectedReturn = 'B';
		char actualReturn = *testList.Get(2);

		cout << "Expected return: " << expectedReturn << endl;
		cout << "Actual return:   " << actualReturn << endl;
		
		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end

	{   // Test begin
		cout << endl << "Test 3 - Create List, PushBack(A, B, C), ShiftRight(4), Get(4)" << endl;
		List<char> testList;
		testList.PushBack('A');
		testList.PushBack('B');
		testList.PushBack('C');
		bool expectedResult = false;
		bool actualResult = testList.ShiftRight(4);
		char *expectedReturn = nullptr;
		char *actualReturn = testList.Get(4);

		cout << "Expected return: nullptr" << endl;
		cout << "Actual return:   ";
		if (actualReturn == nullptr)
			cout << "nullptr" << endl;
		else
			cout << actualReturn << endl;
		
		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end
}

void Tester::Test_ShiftLeft()
{
    DrawLine();
    cout << "TEST: Test_ShiftLeft" << endl;

	{   // Test begin
		cout << endl << "Test 1 - Create List, ShiftLeft(1), Get(1)" << endl;
		List<char> testList;
		bool expectedResult = false;
		bool actualResult = testList.ShiftLeft(1);
		char *expectedReturn = nullptr;
		char *actualReturn = testList.Get(1);

		cout << "Expected return: nullptr" << endl;
		cout << "Actual return:   ";
		if (actualReturn == nullptr)
			cout << "nullptr" << endl;
		else
			cout << actualReturn << endl;
		
		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end

	{   // Test begin
		cout << endl << "Test 2 - Create List, PushBack(A, B, C), ShiftLeft(1), Get(1)" << endl;
		List<char> testList;
		testList.PushBack('A');
		testList.PushBack('B');
		testList.PushBack('C');
		bool expectedResult = true;
		bool actualResult = testList.ShiftLeft(1);
		char expectedReturn = 'C';
		char actualReturn = *testList.Get(1);

		cout << "Expected return: " << expectedReturn << endl;
		cout << "Actual return:   " << actualReturn << endl;
		
		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end

	{   // Test begin
		cout << endl << "Test 3 - Create List, PushBack(A, B, C), ShiftLeft(4), Get(4)" << endl;
		List<char> testList;
		testList.PushBack('A');
		testList.PushBack('B');
		testList.PushBack('C');
		bool expectedResult = false;
		bool actualResult = testList.ShiftLeft(4);
		char *expectedReturn = nullptr;
		char *actualReturn = testList.Get(4);

		cout << "Expected return: nullptr" << endl;
		cout << "Actual return:   ";
		if (actualReturn == nullptr)
			cout << "nullptr" << endl;
		else
			cout << actualReturn << endl;

		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end

}

void Tester::Test_Size()
{
    DrawLine();
    cout << "TEST: Test_Size" << endl;

    {   // Test begin
        cout << endl << "Test 1 - Create List, Size()" << endl;
        List<string> testList;
        int expectedSize = 0;
        int actualSize = testList.Size();

        cout << "Expected size: " << expectedSize << endl;
        cout << "Actual size:   " << actualSize << endl;

		if (expectedSize == actualSize)
			cout << "Pass" << endl;
		else
			cout << "Fail" << endl;
    }   // Test end


	{   // Test begin
		cout << endl << "Test 2 - Create List, PushBack(A, A), Size()" << endl;
		List<string> testList;
		testList.PushBack("A");
		testList.PushBack("A");
		int expectedSize = 2;
		int actualSize = testList.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;
		if (expectedSize == actualSize)
			cout << "Pass" << endl;
		else
			cout << "Fail" << endl;
	}   // Test end

	{   // Test begin
		cout << endl << "Test 3 - Create List, PushBack(A x101), Size()" << endl;
		List<string> testList;
		for (int i = 0; i < 101; i++)
			testList.PushBack("A");

		int expectedSize = 100;
		int actualSize = testList.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (expectedSize == actualSize)
			cout << "Pass" << endl;
		else
			cout << "Fail" << endl;
	}   // Test end

	{   // Test begin
		cout << endl << "Test 4 - Create List, PushBack(A x5), PopFront() x2, Size()" << endl;
		List<char> testList;
		for (int i = 0; i < 5; i++)
			testList.PushBack('A');

		testList.PopFront();
		testList.PopFront();

		int expectedSize = 3;
		int actualSize = testList.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (expectedSize == actualSize)
			cout << "Pass" << endl;
		else
			cout << "Fail" << endl;
	}   // Test end

}

void Tester::Test_IsEmpty()
{
    DrawLine();
    cout << "TEST: Test_IsEmpty" << endl;
	
	{   // Test begin
		cout << endl << "Test 1 - Create List, IsEmpty()" << endl;
		List<char> testList;
		bool expectedResult = true;
		bool actualResult = testList.IsEmpty();

		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end

	{   // Test begin
		cout << endl << "Test 2 - Create List, PushBack(A), IsEmpty()" << endl;
		List<char> testList;
		testList.PushBack('A');
		bool expectedResult = false;
		bool actualResult = testList.IsEmpty();

		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end

	{   // Test begin
		cout << endl << "Test 3 - Create List, PushBack(A x50), IsEmpty()" << endl;
		List<char> testList;
		for (int i = 0; i < 50; i++)
			testList.PushBack('A');

		bool expectedResult = false;
		bool actualResult = testList.IsEmpty();

		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end

	{   // Test begin
		cout << endl << "Test 4 - Create List, PushBack(A x100), IsEmpty()" << endl;
		List<char> testList;
		for (int i = 0; i < 100; i++)
			testList.PushFront('A');

		bool expectedResult = false;
		bool actualResult = testList.IsEmpty();

		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end

	{   // Test begin
		cout << endl << "Test 5 - Create List, PushBack(A, B, C), PopBack() x3, IsEmpty()" << endl;
		List<char> testList;

		testList.PushBack('A');
		testList.PushBack('B');
		testList.PushBack('C');
		testList.PopBack();
		testList.PopBack();
		testList.PopBack();

		bool expectedResult = true;
		bool actualResult = testList.IsEmpty();

		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end
}

void Tester::Test_IsFull()
{
    DrawLine();
    cout << "TEST: Test_IsFull" << endl;

	{   // Test begin
		cout << endl << "Test 1 - Create List, IsFull()" << endl;
		List<string> testList;
	
		bool expectedResult = false;
		bool actualResult = testList.IsFull();

		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end

	{   // Test begin
		cout << endl << "Test 2 - Create List, PushBack(A x5), IsFull()" << endl;
		List<string> testList;
		for (int i = 0; i < 5; i++)
			testList.PushBack("A");

		bool expectedResult = false;
		bool actualResult = testList.IsFull();

		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end

	{   // Test begin
		cout << endl << "Test 3 - Create List, PushBack(A x100), IsFull()" << endl;
		List<char> testList;
		for (int i = 0; i < 100; i++)
			testList.PushBack('A');

		bool expectedResult = true;
		bool actualResult = testList.IsFull();

		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end

	{   // Test begin
		cout << endl << "Test 4 - Create List, PushBack(A x101), IsFull()" << endl;
		List<char> testList;
		for (int i = 0; i < 101; i++)
			testList.PushBack('A');

		bool expectedResult = true;
		bool actualResult = testList.IsFull();

		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end

}

void Tester::Test_PushFront()
{
    DrawLine();
    cout << "TEST: Test_PushFront" << endl;

	{   // Test begin
		cout << endl << "Test 1 - Create List, PushFront(A), GetFront(), Size()" << endl;
		List<char> testList;

		bool expectedResult = true;
		bool actualResult = testList.PushFront('A');
		int expectedSize = 1;
		int actualSize = testList.Size();
		char expectedReturn = 'A';
		char actualReturn = *testList.GetFront();

		cout << "Expected return: " << expectedReturn << endl;
		cout << "Actual return:   " << actualReturn << endl;
		cout << "Expected size:   " << expectedSize << endl;
		cout << "Actual size:     " << actualSize << endl;
		
		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end

	{   // Test begin
		cout << endl << "Test 2 - Create List, PushBack(A x50), PushFront(B), GetFront(), GetSize()" << endl;
		List<char> testList;
		for (int i = 0; i < 50; i++)
			testList.PushBack('A');

		bool expectedResult = true;
		bool actualResult = testList.PushFront('B');
		int expectedSize = 51;
		int actualSize = testList.Size();
		char expectedReturn = 'B';
		char actualReturn = *testList.GetFront();

		cout << "Expected return: " << expectedReturn << endl;
		cout << "Actual return:   " << actualReturn << endl;
		cout << "Expected size:   " << expectedSize << endl;
		cout << "Actual size:     " << actualSize << endl;
		
		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end

	{   // Test begin
		cout << endl << "Test 3 - Create List, PushBack(B x100), PushFront(C), GetFront(), GetSize()" << endl;
		List<char> testList;
		for (int i = 0; i < 100; i++)
			testList.PushBack('B');

		bool expectedResult = false;
		bool actualResult = testList.PushFront('C');
		int expectedSize = 100;
		int actualSize = testList.Size();
		char expectedReturn = 'B';
		char actualReturn = *testList.GetFront();

		cout << "Expected return: " << expectedReturn << endl;
		cout << "Actual return:   " << actualReturn << endl;
		cout << "Expected size:   " << expectedSize << endl;
		cout << "Actual size:     " << actualSize << endl;
		
		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end

}

void Tester::Test_PushBack()
{
    DrawLine();
    cout << "TEST: Test_PushBack" << endl;

	{   // Test begin
		cout << endl << "Test 1 - Create List, PushBack(A), GetBack(), Size()" << endl;
		List<char> testList;

		bool expectedResult = true;
		bool actualResult = testList.PushBack('A');
		int expectedSize = 1;
		int actualSize = testList.Size();
		char expectedReturn = 'A';
		char actualReturn = *testList.GetBack();

		cout << "Expected return: " << expectedReturn << endl;
		cout << "Actual return:   " << actualReturn << endl;
		cout << "Expected size:   " << expectedSize << endl;
		cout << "Actual size:     " << actualSize << endl;
		
		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end

	{   // Test begin
		cout << endl << "Test 2 - Create List, PushBack(A x50), PushBack(B), GetBack(), Size()" << endl;
		List<char> testList;
		for (int i = 0; i < 50; i++)
			testList.PushBack('A');

		bool expectedResult = true;
		bool actualResult = testList.PushBack('B');
		int expectedSize = 51;
		int actualSize = testList.Size();
		char expectedReturn = 'B';
		char actualReturn = *testList.GetBack();

		cout << "Expected return: " << expectedReturn << endl;
		cout << "Actual return:   " << actualReturn << endl;
		cout << "Expected size:   " << expectedSize << endl;
		cout << "Actual size:     " << actualSize << endl;
		
		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end

	{   // Test begin
		cout << endl << "Test 3 - Create List, PushBack(B x100), PushBack(C), GetBack(), Size()" << endl;
		List<char> testList;
		for (int i = 0; i < 100; i++)
			testList.PushBack('B');

		bool expectedResult = false;
		bool actualResult = testList.PushBack('C');
		int expectedSize = 100;
		int actualSize = testList.Size();
		char expectedReturn = 'B';
		char actualReturn = *testList.GetBack();

		cout << "Expected return: " << expectedReturn << endl;
		cout << "Actual return:   " << actualReturn << endl;
		cout << "Expected size:   " << expectedSize << endl;
		cout << "Actual size:     " << actualSize << endl;
		
		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end
}

void Tester::Test_PopFront()
{
    DrawLine();
    cout << "TEST: Test_PopFront" << endl;

	{   // Test begin
		cout << endl << "Test 1 - Create List, PopFront(), GetFront(), Size()" << endl;
		List<char> testList;

		bool expectedResult = false;
		bool actualResult = testList.PopFront();
		int expectedSize = 0;
		int actualSize = testList.Size();
		char* expectedReturn = nullptr;
		char* actualReturn = testList.GetFront();

		cout << "Expected return: nullptr" << endl;
		cout << "Actual return:   ";
		if (actualReturn == nullptr)
			cout << "nullptr" << endl;
		else
			cout << actualReturn << endl;
		cout << "Expected size:   " << expectedSize << endl;
		cout << "Actual size:     " << testList.Size() << endl;
		
		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end

	{   // Test begin
		cout << endl << "Test 2 - Create List, PushBack(A, B), PopFront(), GetFront(), Size()" << endl;
		List<char> testList;
		testList.PushBack('A');
		testList.PushBack('B');

		bool expectedResult = true;
		bool actualResult = testList.PopFront();
		int expectedSize = 1;
		int actualSize = testList.Size();
		char expectedReturn = 'B';
		char actualReturn = *testList.GetFront();

		cout << "Expected return: " << expectedReturn << endl;
		cout << "Actual return:   " << actualReturn << endl;
		cout << "Expected size:   " << expectedSize << endl;
		cout << "Actual size:     " << testList.Size() << endl;
		
		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end

	{   // Test begin
		cout << endl << "Test 3 - Create List, PushBack(B, A), PopFront(), GetFront(), Size()" << endl;
		List<char> testList;
		testList.PushBack('B');
		testList.PushBack('A');

		bool expectedResult = true;
		bool actualResult = testList.PopFront();
		int expectedSize = 1;
		int actualSize = testList.Size();
		char expectedReturn = 'A';
		char actualReturn = *testList.GetFront();

		cout << "Expected return: " << expectedReturn << endl;
		cout << "Actual return:   " << actualReturn << endl;
		cout << "Expected size:   " << expectedSize << endl;
		cout << "Actual size:     " << testList.Size() << endl;

		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end

}

void Tester::Test_PopBack()
{
    DrawLine();
    cout << "TEST: Test_PopBack" << endl;

	{   // Test begin
		cout << endl << "Test 1 - Create List, PopBack(), GetBack(), Size()" << endl;
		List<char> testList;
		
		bool expectedResult = false;
		bool actualResult = testList.PopBack();
		int expectedSize = 0;
		int actualSize = testList.Size();
		char* expectedReturn = nullptr;
		char* actualReturn = testList.GetBack();

		cout << "Expected return: nullptr" << endl;
		cout << "Actual return:   ";
		if (actualReturn == nullptr)
			cout << "nullptr" << endl;
		else
			cout << actualReturn << endl;
		cout << "Expected size:   " << expectedSize << endl;
		cout << "Actual size:     " << testList.Size() << endl;
		
		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end

	{   // Test begin
		cout << endl << "Test 2 - Create List, PushBack(A, B), PopBack(), GetBack(), Size()" << endl;
		List<char> testList;
		testList.PushBack('A');
		testList.PushBack('B');

		bool expectedResult = true;
		bool actualResult = testList.PopBack();
		int expectedSize = 1;
		int actualSize = testList.Size();
		char expectedReturn = 'A';
		char actualReturn = *testList.GetBack();

		cout << "Expected return: " << expectedReturn << endl;
		cout << "Actual return:   " << actualReturn << endl;
		cout << "Expected size:   " << expectedSize << endl;
		cout << "Actual size:     " << testList.Size() << endl;

		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end

	{   // Test begin
		cout << endl << "Test 3 - Create List, PushBack(B, A), PopBack(), GetBack(), Size()" << endl;
		List<char> testList;
		testList.PushBack('B');
		testList.PushBack('A');

		bool expectedResult = true;
		bool actualResult = testList.PopBack();
		int expectedSize = 1;
		int actualSize = testList.Size();
		char expectedReturn = 'B';
		char actualReturn = *testList.GetBack();

		cout << "Expected return: " << expectedReturn << endl;
		cout << "Actual return:   " << actualReturn << endl;
		cout << "Expected size:   " << expectedSize << endl;
		cout << "Actual size:     " << testList.Size() << endl;

		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end

}

void Tester::Test_Clear()
{
    DrawLine();
    cout << "TEST: Test_Clear" << endl;

	{   // Test begin
		cout << endl << "Test 1 - Create List, Clear(), Get(0), Size()" << endl;
		List<char> testList;
		testList.Clear();
		int expectedSize = 0;
		int actualSize = testList.Size();
		char* expectedReturn = nullptr;
		char* actualReturn = testList.Get(0);

		cout << "Expected return: nullptr" << endl;
		cout << "Actual return:   ";
		if (actualReturn == nullptr)
			cout << "nullptr" << endl;
		else
			cout << actualReturn << endl;
		cout << "Expected size:   " << expectedSize << endl;
		cout << "Actual size:     " << actualSize << endl;

		checkPass(expectedReturn, actualReturn);
	}   // Test end

	{   // Test begin
		cout << endl << "Test 2 - Create List, PushBack(A, B, C), Clear(), Get(0), Size()" << endl;
		List<char> testList;
		testList.PushBack('A');
		testList.PushBack('B');
		testList.PushBack('C');
		testList.Clear();
		int expectedSize = 0;
		int actualSize = testList.Size();

		char* expectedReturn = nullptr;
		char* actualReturn = testList.Get(0);

		cout << "Expected return: nullptr" << endl;
		cout << "Actual return:   ";
		if (actualReturn == nullptr)
			cout << "nullptr" << endl;
		else
			cout << actualReturn << endl;
		cout << "Expected size:   " << expectedSize << endl;
		cout << "Actual size:     " << actualSize << endl;

		checkPass(expectedSize, actualSize);
	}   // Test end

}

void Tester::Test_Get()
{
    DrawLine();
    cout << "TEST: Test_Get" << endl;

	{   // Test begin
		cout << endl << "Test 1 - Create List, Get(0)" << endl;
		List<char> testList;
		char* expectedReturn = nullptr;
		char* actualReturn = testList.Get(0);

		cout << "Expected return: nullptr" << endl;
		cout << "Actual return:   ";
		if (actualReturn == nullptr)
			cout << "nullptr" << endl;
		else
			cout << actualReturn << endl;

		checkPass(expectedReturn, actualReturn);

	}   // Test end

	{   // Test begin
		cout << endl << "Test 2 - Create List, PushBack(A, B, C), Get(2)" << endl;
		List<char> testList;
		testList.PushBack('A');
		testList.PushBack('B');
		testList.PushBack('C');
		char expectedReturn = 'C';
		char actualReturn = *testList.Get(2);

		cout << "Expected return: " << expectedReturn << endl;
		cout << "Actual return:   " << actualReturn << endl;

		checkPass(expectedReturn, actualReturn);
	}   // Test end

	{   // Test begin
		cout << endl << "Test 3 - Create List, PushBack(A, B, C), Get(4)" << endl;
		List<char> testList;
		testList.PushBack('A');
		testList.PushBack('B');
		testList.PushBack('C');

		char* expectedReturn = nullptr;
		char* actualReturn = testList.Get(4);

		cout << "Expected return: nullptr" << endl;
		cout << "Actual return:   ";
		if (actualReturn == nullptr)
			cout << "nullptr" << endl;
			//checkNull();
		
		else
			cout << actualReturn << endl;

		checkPass(expectedReturn, actualReturn);
	}   // Test end

}

void Tester::Test_GetFront()
{
    DrawLine();
    cout << "TEST: Test_GetFront" << endl;

	{   // Test begin
		cout << endl << "Test 1 - Create List, GetFront()" << endl;
		List<char> testList;
		char* expectedReturn = nullptr;
		char* actualReturn = testList.GetFront();

		cout << "Expected return: nullptr" << endl;
		cout << "Actual return:   ";
		if (actualReturn == nullptr)
			cout << "nullptr" << endl;
		else
			cout << *actualReturn << endl;

		checkPass(expectedReturn, actualReturn);
	}   // Test end

	{   // Test begin
		cout << endl << "Test 2 - Create List, PushBack(A, B, C), GetFront()" << endl;
		List<char> testList;
		testList.PushBack('A');
		testList.PushBack('B');
		testList.PushBack('C');
		char expectedReturn = 'A';
		char* actualReturn = testList.GetFront();

		cout << "Expected return: ";
		cout << expectedReturn << endl;

		cout << "Actual return:   ";
		if (actualReturn == nullptr)
			cout << "nullptr" << endl;
		else
			cout << *actualReturn << endl;

		checkPass(expectedReturn, *actualReturn);
	}   // Test end

	{   // Test begin
		cout << endl << "Test 3 - Create List, PushFront(A, B, C), GetFront()" << endl;
		List<char> testList;
		testList.PushFront('A');
		testList.PushFront('B');
		testList.PushFront('C');
		char expectedReturn = 'C';
		char* actualReturn = testList.GetFront();

		cout << "Expected return: ";
		cout << expectedReturn << endl;

		cout << "Actual return:   ";
		if (actualReturn == nullptr)
			cout << "nullptr" << endl;
		else
			cout << *actualReturn << endl;

		checkPass(expectedReturn, *actualReturn);
	}   // Test end
	
}

void Tester::Test_GetBack()
{
    DrawLine();
    cout << "TEST: Test_GetBack" << endl;

	{   // Test begin
		cout << endl << "Test 1 - Create List, GetBack()" << endl;
		List<char> testList;
		char* expectedReturn = nullptr;
		char* actualReturn = testList.GetBack();

		cout << "Expected return: nullptr" << endl;
		cout << "Actual return:   ";
		if (actualReturn == nullptr)
			cout << "nullptr" << endl;
		else
			cout << *actualReturn << endl;

		checkPass(expectedReturn, actualReturn);

	}   // Test end

	{   // Test begin
		cout << endl << "Test 2 - Create List, PushBack(A, B, C), GetBack()" << endl;
		List<char> testList;
		testList.PushBack('A');
		testList.PushBack('B');
		testList.PushBack('C');
		char expectedReturn = 'C';
		char* actualReturn = testList.GetBack();

		cout << "Expected return: ";
		cout << expectedReturn << endl;

		cout << "Actual return:   ";
		if (actualReturn == nullptr)
			cout << "nullptr" << endl;
		else
			cout << *actualReturn << endl;

		checkPass(expectedReturn, *actualReturn);
	}   // Test end

	{   // Test begin
		cout << endl << "Test 3 - Create List, PushFront(A, B, C) GetBack()" << endl;
		List<char> testList;
		testList.PushFront('A');
		testList.PushFront('B');
		testList.PushFront('C');
		char expectedReturn = 'A';
		char* actualReturn = testList.GetBack();

		cout << "Expected return: ";
		cout << expectedReturn << endl;

		cout << "Actual return:   ";
		if (actualReturn == nullptr)
			cout << "nullptr" << endl;
		else
			cout << *actualReturn << endl;

		checkPass(expectedReturn, *actualReturn);

	}   // Test end
}

void Tester::Test_GetCountOf()
{
    DrawLine();
    cout << "TEST: Test_GetCountOf" << endl;

	{   // Test begin
		cout << endl << "Test 1 - Create List, GetCountOf(A)" << endl;
		List<char> testList;
		int expectedReturn = 0;
		int actualReturn = testList.GetCountOf('A');

		cout << "Expected return: " << expectedReturn << endl;
		cout << "Actual return:   " << actualReturn << endl;

		checkPass(expectedReturn, actualReturn);
	}   // Test end

	{   // Test begin
		cout << endl << "Test 2 - Create List, PushBack(A x25, B x75), GetCountOf(B)" << endl;
		List<char> testList;
		for (int i = 0; i < 100; i++)
		{
			if (i >= 25)
				testList.PushBack('B');
			else
				testList.PushBack('A');
		}

		int expectedReturn = 75;
		int actualReturn = testList.GetCountOf('B');

		cout << "Expected return: " << expectedReturn << endl;
		cout << "Actual return:   " << actualReturn << endl;

		checkPass(expectedReturn, actualReturn);

	}   // Test end

	{   // Test begin
		cout << endl << "Test 3 - Create List, PushBack(A, B, C), GetCountOf(D)" << endl;
		List<char> testList;
		testList.PushBack('A');
		testList.PushBack('B');
		testList.PushBack('C');

		int expectedReturn = 0;
		int actualReturn = testList.GetCountOf('D');

		cout << "Expected return: " << expectedReturn << endl;
		cout << "Actual return:   " << actualReturn << endl;
		checkPass(expectedReturn, actualReturn);
	}   // Test end

	{   // Test begin
		cout << endl << "Test 4 - Create List, PushBack(A x105), GetCountOf(A)" << endl;
		List<char> testList;
		for (int i = 0; i < 105; i++)
			testList.PushBack('A');

		int expectedReturn = 100;
		int actualReturn = testList.GetCountOf('A');

		cout << "Expected return: " << expectedReturn << endl;
		cout << "Actual return:   " << actualReturn << endl;

		checkPass(expectedReturn, actualReturn);
	}   // Test end

}

void Tester::Test_Contains()
{
    DrawLine();
    cout << "TEST: Test_Contains" << endl;

	{   // Test begin
		cout << endl << "Test 1 - Create List, Contains(A)" << endl;
		List<char> testList;
		bool expectedResult = false;
		bool actualResult = testList.Contains('A');

		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}  // Test end

	{   // Test begin
		cout << endl << "Test 2 - Create List, PushBack(A x25, B x75) Contains(B)" << endl;
		List<char> testList;
		for (int i = 0; i < 100; i++)
		{
			if (i < 25)
				testList.PushBack('A');
			else
				testList.PushBack('B');
		}
		
		bool expectedResult = true;
		bool actualResult = testList.Contains('B');

		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end

	{   // Test begin
		cout << endl << "Test 3 - Create List, PushBack(A, B, C) Contains(D)" << endl;
		List<char> testList;
			testList.PushBack('A');
			testList.PushBack('B');
			testList.PushBack('C');

		bool expectedResult = false;
		bool actualResult = testList.Contains('D');

		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end

	{   // Test begin
		cout << endl << "Test 4 - Create List, PushBack(A x105) Contains(A)" << endl;
		List<char> testList;
		for (int i = 0; i < 105; i++)
			testList.PushFront('A');

		bool expectedResult = true;
		bool actualResult = testList.Contains('A');

		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end

}

void Tester::Test_Remove()
{
    DrawLine();
    cout << "TEST: Test_Remove" << endl;

	{   // Test begin
		cout << endl << "Test 1 - Create List, Remove(A), GetFront(), Size()" << endl;
		List<char> testList;

		bool expectedResult = false;
		bool actualResult = testList.Remove('A');
		char *expectedReturn = nullptr;
		char *actualReturn = testList.GetFront();
		int expectedSize = 0;
		int actualSize = testList.Size();

		cout << "Expected return: nullptr" << endl;
		cout << "Actual return:   ";
		if (actualReturn == nullptr)
			cout << "nullptr" << endl;
		else
			cout << *actualReturn << endl;
		cout << "Expected size:   " << expectedSize << endl;
		cout << "Actual size:     " << testList.Size() << endl;
		
		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end

	{   // Test begin
		cout << endl << "Test 2 - Create List, PushBack(A, B, C), Remove(D), GetFront(), Size()" << endl;
		List<char> testList;
		testList.PushBack('A');
		testList.PushBack('B');
		testList.PushBack('C');

		bool expectedResult = false;
		bool actualResult = testList.Remove('D');
		char expectedReturn = 'A';
		char actualReturn = *testList.Get(0);
		int expectedSize = 3;
		int actualSize = testList.Size();

		cout << "Expected return: " << expectedReturn << endl;
		cout << "Actual return:   " << actualReturn << endl;
		cout << "Expected size:   " << expectedSize << endl;
		cout << "Actual size:     " << testList.Size() << endl;
		
		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end

	{   // Test begin
		cout << endl << "Test 3 - Create List, PushBack(A, B, C, B), Remove(B), Get(1), Size()" << endl;
		List<char> testList;
		testList.PushBack('A');
		testList.PushBack('B');
		testList.PushBack('C');
		testList.PushBack('B');

		bool expectedResult = true;
		bool actualResult = testList.Remove('B');
		int expectedSize = 2;
		int actualSize = testList.Size();
		char expectedReturn = 'C';
		char actualReturn = *testList.Get(1);

		cout << "Expected return: " << expectedReturn << endl;
		cout << "Actual return:   " << actualReturn << endl;
		cout << "Expected size:   " << expectedSize << endl;
		cout << "Actual size:     " << testList.Size() << endl;
		
		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end

	{   // Test begin
		cout << endl << "Test 4 - Create List, PushBack(A x25, B x50, C x25), Remove(B), Get(26), Size()" << endl;
		List<char> testList;
		for (int i = 0; i < 100; i++)
		{ 
		if (i < 25)
			testList.PushBack('A');
		else if (i < 75)
			testList.PushBack('B');
		else
			testList.PushBack('C');
		}

		bool expectedResult = true;
		bool actualResult = testList.Remove('B');
		int expectedSize = 50;
		int actualSize = testList.Size();
		char expectedReturn = 'C';
		char actualReturn = *testList.Get(25);

		cout << "Expected return: " << expectedReturn << endl;
		cout << "Actual return:   " << actualReturn << endl;
		cout << "Expected size:   " << expectedSize << endl;
		cout << "Actual size:     " << testList.Size() << endl;
		
		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end
}

void Tester::Test_Insert()
{
	DrawLine();
	cout << "TEST: Test_Insert" << endl;

	{   // Test begin
		cout << endl << "Test 1 - Create List, Insert(0, A), GetFront(), Size()" << endl;
		List<char> testList;
		bool expectedResult = true;
		bool actualResult = testList.Insert(0, 'A');
		int expectedSize = 1;
		int actualSize = testList.Size();
		char expectedReturn = 'A';
		char actualReturn = *testList.GetFront();

		cout << "Expected return: " << expectedReturn << endl;
		cout << "Actual return:   " << actualReturn << endl;
		cout << "Expected size:   " << expectedSize << endl;
		cout << "Actual size:     " << testList.Size() << endl;
		
		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end

	{   // Test begin
		cout << endl << "Test 2 - Create List, PushBack(A, B, C), Insert(1, D), Get(1), Size()" << endl;
		List<char> testList;
		testList.PushBack('A');
		testList.PushBack('B');
		testList.PushBack('C');
		bool expectedResult = true;
		bool actualResult = testList.Insert(1, 'D');
		int expectedSize = 4;
		int actualSize = testList.Size();
		char expectedReturn = 'D';
		char actualReturn = *testList.Get(1);

		cout << "Expected return: " << expectedReturn << endl;
		cout << "Actual return:   " << actualReturn << endl;
		cout << "Expected size:   " << expectedSize << endl;
		cout << "Actual size:     " << testList.Size() << endl;

		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end

	{   // Test begin
		cout << endl << "Test 3 - Create List, PushBack(A x100), Insert(1, D), Get(1), Size()" << endl;
		List<char> testList;
		for (int i = 0; i < 100; i++)
			testList.PushBack('A');

		bool expectedResult = false;
		bool actualResult = testList.Insert(1, 'D');
		int expectedSize = 100;
		int actualSize = testList.Size();
		char expectedReturn = 'A';
		char actualReturn = *testList.Get(1);

		cout << "Expected return: " << expectedReturn << endl;
		cout << "Actual return:   " << actualReturn << endl;
		cout << "Expected size:   " << expectedSize << endl;
		cout << "Actual size:     " << testList.Size() << endl;

		checkResult(expectedResult, actualResult);
		checkPass(expectedResult, actualResult);
	}   // Test end

}

void checkPass(char expected, char actual)
{
	if (expected == actual)
		cout << "Pass" << endl;
	else
		cout << "Fail" << endl;
}

void checkPass(char* expected, char* actual)
{
	if (expected == actual)
		cout << "Pass" << endl;
	else
		cout << "Fail" << endl;
}

void checkResult(bool expected, bool actual)
{
	if (expected == true)
	{
		cout << "Expected result: True" << endl;
		if (actual == true)
			cout << "Actual result:   True" << endl;
		else
			cout << "Actual result:   False" << endl;
	}
	else
	{
		cout << "Expected result: False" << endl;
		if (actual == false)
			cout << "Actual result:   False" << endl;
		else
			cout << "Actual result:   True" << endl;
	}
}