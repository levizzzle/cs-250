#ifndef _LIST_HPP
#define _LIST_HPP

const int ARRAY_SIZE = 100;
	
template <typename T>
class List
{
private:
    // private member variables
    int m_itemCount = 0;
	T m_arr[ARRAY_SIZE];

    // functions for interal-workings
    bool ShiftRight( int atIndex )
    {
		if (atIndex < 0 || atIndex > m_itemCount)
			return false;
		else
		{
			for (int i = m_itemCount; i >= atIndex; i--)
				m_arr[i] = m_arr[i - 1];
			return true;
		}
	}

    bool ShiftLeft( int atIndex )
    {
        if(atIndex < 0 || atIndex > m_itemCount)
			return false;
		else
		{
			for (int i = atIndex; i < m_itemCount; i++)
				m_arr[i] = m_arr[i + 1];
			return true;
		}
    }

public:
    List()
    {
    }

    ~List()
    {
    }

    // Core functionality
    int     Size() const
    {
			return m_itemCount; // placeholder
    }


    bool    IsEmpty() const
    {
		if (m_itemCount == 0)
			return true; 
		else
			return false;
    }

    bool    IsFull() const
    {
		if (m_itemCount == ARRAY_SIZE)
			return true; // placeholder
		else
			return false;

    }

    bool    PushFront( const T& newItem )
    {
		if (m_itemCount >= ARRAY_SIZE)
		{
			return false;
		}
		else
		{
			int m_counter = m_itemCount;
			for (int i = 0; i < m_counter; m_counter--)
				m_arr[m_counter] = m_arr[m_counter - 1];

			m_arr[0] = newItem;
			m_itemCount++;
			return true; 
		}
			
    }

    bool    PushBack( const T& newItem )
    {
		if (m_itemCount >= ARRAY_SIZE)
			return false;
		else
		{
			m_arr[m_itemCount] = newItem;
			m_itemCount++;
			return true; 
		}
		return true;
    }

    bool    Insert( int atIndex, const T& item )
    {
		if (atIndex < 0 || atIndex > m_itemCount || IsFull())
			return false;
		else
		{
			for (int i = m_itemCount; i > atIndex; i--)
				m_arr[i] = m_arr[i - 1];
			m_arr[atIndex] = item;
			m_itemCount++;
			return true;
		}
    }

    bool    PopFront()
    {
		if (IsEmpty())
			return false;
		else
		{
			for (int i = 0; i < m_itemCount; i++)
			{
				m_arr[i] = m_arr[i + 1];
			}
			m_itemCount--;
			return true;
		}
    }

	bool    PopBack()
	{
		if (IsEmpty())
			return false;
		else
			ShiftLeft(m_itemCount);
		m_itemCount--;
		return true;
    }

    bool    Remove( const T& item )
    {
		int j = 0;

		if (IsEmpty())
			return false;
		else
		{
			int temp_arr[ARRAY_SIZE];

				for (int i = 0; i < m_itemCount; i++)
				{
					if (m_arr[i] == item)
					{
						temp_arr[j] = i;
						j++;
					}
				}
				for (int i = j - 1; i >= 0; i--)
					Remove(temp_arr[i]);

				if (j == 0)
					return false;
				else
					return true;
		}
    }

    bool    Remove( int atIndex )
    {
		if (IsEmpty())
			return false;
		else
		{
				ShiftLeft(atIndex);
				m_itemCount--;
				return true;
		}
    }

    void    Clear()
    {
		int count = m_itemCount;
		for (int i = 0; i < count; i++)
		{
			m_arr[i] = m_arr[count + 1];
			m_itemCount--;
		}
    }

    // Accessors
    T*      Get( int atIndex )
    {
		if (IsEmpty() || atIndex > m_itemCount)
			return nullptr;
		return &m_arr[atIndex]; 
    }

    T*      GetFront()
    {
		if (IsEmpty())
			return nullptr;
		return &m_arr[0];
    }

    T*      GetBack()
    {
		if (IsEmpty())
			return nullptr;
		return &m_arr[m_itemCount -1];
    }

    // Additional functionality
    int     GetCountOf( const T& item ) const
    {
		int count = 0;

		for (int i = 0; i < m_itemCount; i++)
			if (m_arr[i] == item) count++;

		return count;
    }

    bool    Contains( const T& item ) const
    {
		return (GetCountOf(item) > 0);
    }

    friend class Tester;
};


#endif
