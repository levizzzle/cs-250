#include "CourseCatalog.hpp"

#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

#include "UTILITIES/Menu.hpp"
#include "EXCEPTIONS/CourseNotFoundException.hpp"
#include "DATA_STRUCTURES/Stack.hpp"

CourseCatalog::CourseCatalog()
{
    LoadCourses();
}

void CourseCatalog::LoadCourses() 
{
    Menu::Header( "LOADING COURSES" );
    ifstream input( "courses.txt" );

    if ( !input.is_open() )
    {
        cout << "Error opening input text file, courses.txt" << endl;
        return;
    }

    string label, courseCode, courseName, prerequisite;
    Course newCourse;

    while ( input >> label )
    {
        if ( label == "COURSE" )
        {
            if ( newCourse.name != "" )
            {
                m_courses.PushBack( newCourse );
                newCourse.Clear();
            }

            input >> newCourse.code >> newCourse.name;
        }
        else if ( label == "PREREQ" )
        {
            input >> newCourse.prereq;
        }
    }

    input.close();

    cout << " * " << m_courses.Size() << " courses loaded" << endl << endl;
}

void CourseCatalog::ViewCourses() noexcept
{
    Menu::Header( "VIEW COURSES" );
	Menu::DrawHorizontalBar(80);
	Course course;

	cout << setw(55) << "     COURSE" << setw(15) << "CODE" << setw(10) << "PREREQ" << "|" << endl;
	Menu::DrawHorizontalBar(80);

	for (int i = 0; i < m_courses.Size(); i++) 
	{
		Course course = m_courses.Get(i);
		cout << setw(3) << i+1 << "- " << setw(50) << course.name
			<< setw(15) << course.code 
			<< setw(10) << course.prereq << "|" << endl;
	} 
	Menu::DrawHorizontalBar(80);
	cout << endl;
}

Course CourseCatalog::FindCourse(const string& code)
{
	for (int i = 0; i < m_courses.Size(); i++)
	{
		if (code == m_courses.Get(i).code)
			return m_courses.Get(i);
	}

	throw CourseNotFound("Could not find course - ");
		
	cout << endl;
	Menu::DrawHorizontalBar(80);
	cout << endl;
}

void CourseCatalog::ViewPrereqs() noexcept
{
	Menu::Header("GET PREREQS");
	LinkedStack<Course> prereqs;
	string code;
	Course course;
	int numPrereqs = 1;

	cout << "Enter a Course Code: ";
	cin >> code;

	try
	{
		course = FindCourse(code);
		if (course.prereq == "")
		{
			cout << "No Prerequisites" << endl << endl;
			return;
		}
	}
	catch (CourseNotFound& e)
	{
		cout << e.what() << code << endl;
		return;
	}
	
	if (course.prereq == "")
		cout << "No Prerequisites";
	else
		while (course.prereq != "")
		{
			prereqs.Push(FindCourse(course.prereq));
			course = FindCourse(course.prereq);
		}

	try
	{
		course = FindCourse(code);
	}
	catch (CourseNotFound& e)
	{
		cout << "Invalid code - " << e.what();
		return;
	}

	cout << endl << "Classes Required:" << endl;
	while (prereqs.isEmpty() == false)
	{
		cout << numPrereqs << ". " << prereqs.Top().code << " - " << prereqs.Top().name << endl;
		prereqs.Pop();
		numPrereqs++;
	}
	cout << endl;
}

void CourseCatalog::Run()
{
    bool done = false;
    while ( !done )
    {
        Menu::Header( "MAIN MENU" );

        int choice = Menu::ShowIntMenuWithPrompt( { "View all courses", "Get course prerequisites", "Exit" } );

        switch( choice )
        {
            case 1:
                ViewCourses();
            break;

            case 2:
                ViewPrereqs();
            break;

            case 3:
                done = true;
            break;
        }
    }
}
