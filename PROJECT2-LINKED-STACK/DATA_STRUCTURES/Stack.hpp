#ifndef _STACK_HPP
#define _STACK_HPP

#include "Node.hpp"

template <typename T>
class LinkedStack 
{
    public:
    LinkedStack()
    {
		m_itemCount = 0;
    }

    void Push( const T& newData )
    {
		Node<T>* newNode = new Node<T>;
		newNode->data = newData;

		if (isEmpty())
		{
			m_ptrFirst = newNode;
			m_ptrFirst->ptrPrev = nullptr;
			m_ptrLast = newNode;
			m_ptrLast->ptrNext = nullptr;
		}
		else
		{
			m_ptrLast->ptrNext = newNode;
			newNode->ptrPrev = m_ptrLast;
			m_ptrLast = newNode;
			m_ptrLast->ptrNext = nullptr;
		}
		m_itemCount++; 
    }

    T& Top()
    {
		if (isEmpty())
			throw out_of_range("Nothing in stack!");
		else
			return m_ptrLast->data;
	}

    void Pop()
    {
		if (isEmpty())
			return;
		else if (m_ptrFirst == m_ptrLast)
		{
			delete m_ptrFirst;
			m_ptrFirst = nullptr;
			m_ptrLast = nullptr;
		}
		else
		{
			Node<T>* ptrSecondToLast = m_ptrLast->ptrPrev;
			delete m_ptrLast;
			m_ptrLast = ptrSecondToLast;
			m_ptrLast->ptrNext = nullptr;
		}
		m_itemCount--;
    }

    int Size()
    {
        return m_itemCount;    
    }

	bool isEmpty()
	{
		return (m_itemCount == 0);
	}

    private:
    Node<T>* m_ptrFirst;
    Node<T>* m_ptrLast;
    int m_itemCount;
};

#endif
